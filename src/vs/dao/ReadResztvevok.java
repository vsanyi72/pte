package vs.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import vs.model.Resztvevo;

public class ReadResztvevok implements Read<Resztvevo>{

	@Override
	public ArrayList<Resztvevo> ReadText() throws IOException {
		ArrayList<Resztvevo> resztvevok = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(new File("d://resztvevok.txt")));
		String nev="";
		String azonosito="";
		int penz = 0;
		Random rnd = new Random();
		String sor;
		while ((sor=br.readLine())!=null) {
			String[] szavak = sor.split(";");
			nev= szavak[0];
			if (szavak.length>1) {
				azonosito = szavak[1];
			}
			penz = rnd.nextInt(10000)+5000;
			Resztvevo resztvevo = new Resztvevo(nev, azonosito, penz);
			resztvevok.add(resztvevo);
			
		}
		br.close();
		return resztvevok;
	}

}
