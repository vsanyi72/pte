package vs.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import vs.model.Rendezveny;

public class ReadRendezvenyek implements Read<Rendezveny> {
	
	

	@Override
	public ArrayList<Rendezveny> ReadText() throws IOException {
		ArrayList<Rendezveny> rendezvenyek = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(new File("d://rendezvenyek.txt")));
		String cim="";
		String idopont="";
		int jegyar=0;
		String sor;
		while ((sor=br.readLine())!=null) {
			cim= sor.split(";")[0];
			idopont= sor.split(";")[1];
			jegyar = Integer.valueOf(sor.split(";", jegyar)[2]);
			Rendezveny rendezveny = new Rendezveny(cim, idopont, jegyar);
			rendezvenyek.add(rendezveny);
		}
		br.close();
		return rendezvenyek;
	}

}
