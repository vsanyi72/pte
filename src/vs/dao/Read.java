package vs.dao;

import java.io.IOException;
import java.util.ArrayList;

public interface Read<E> {

	public ArrayList<E> ReadText() throws IOException; 
	
}
