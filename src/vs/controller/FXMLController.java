package vs.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import vs.dao.ReadRendezvenyek;
import vs.dao.ReadResztvevok;
import vs.model.Rendezveny;
import vs.model.Resztvevo;

public class FXMLController implements Initializable {

	ArrayList<Rendezveny> rendezvenyek = new ArrayList<>();
	ArrayList<Resztvevo> resztvevok = new ArrayList<>();

	@FXML
	private ListView<Rendezveny> lvRendezvenyek;

	@FXML
	private ListView<Resztvevo> lvResztvevok;

	@FXML
	private Button btnJubileum;

	@FXML
	private Label lbLatogatott;

	@FXML
	private Label lbKijelolt;

	@FXML
	private ListView<Rendezveny> lvLatogatottRendezvenyek;

	@FXML
	private ListView<Rendezveny> lvLBRendezvenyek;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// lista beolvasás és listView-ek feltöltése
		ReadRendezvenyek rn = new ReadRendezvenyek();
		ReadResztvevok rv = new ReadResztvevok();
		try {
			rendezvenyek = rn.ReadText();
			resztvevok = rv.ReadText();
			ObservableList<Rendezveny> olRendezveny = FXCollections.observableArrayList(rendezvenyek);
			lvRendezvenyek.setItems(olRendezveny);
			ObservableList<Resztvevo> olResztvevok = FXCollections.observableArrayList(resztvevok);
			lvResztvevok.setItems(olResztvevok);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		lvRendezvenyek.setOnMouseClicked((new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				lbKijelolt.setText("A kijelölt rendezvény összbevétele : "
						+ lvRendezvenyek.getSelectionModel().getSelectedItem().getOsszBevetel());

			}
		}));

		lvLBRendezvenyek.setOnMouseClicked((new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				lbKijelolt.setText("A kijelölt rendezvény összbevétele : "
						+ lvLBRendezvenyek.getSelectionModel().getSelectedItem().getOsszBevetel());

			}
		}));

		lvResztvevok.setOnMouseClicked((new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				lbLatogatott.setText(lvResztvevok.getSelectionModel().getSelectedItem().getNev()
						+ " által látogatott rendezvények :");
				ObservableList<Rendezveny> olSzemelyRendezvenyei = FXCollections.observableArrayList(
						lvResztvevok.getSelectionModel().getSelectedItem().getResztVettRendezvenyek());
				lvLatogatottRendezvenyek.setItems(olSzemelyRendezvenyei);

			}
		}));

	}

	public void pressedJubileum() {
		btnJubileum.setDisable(true);
		Random rnd = new Random();
		for (Rendezveny rendezveny : rendezvenyek) {
			for (Resztvevo resztvevo : resztvevok) {
				if (rnd.nextInt(100) > 20) {
					resztvevo.getResztVettRendezvenyek().add(rendezveny);
					resztvevo.penztKolt(rendezveny.getJegyar());
					rendezveny.resztVesz();
				}
			}
		}
		lvResztvevok.refresh();
		// lista 3 legtobb bevetelt hozo rendezvenyrol, nev + letszam, csokkeno letszam
		int legtobbBevetel = 0;
		int elozoLegtobbBevetel = 50000;
		Rendezveny legNagyobbRendezveny = null;
		ArrayList<Rendezveny> haromRendezveny = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			for (Rendezveny rendezveny : rendezvenyek) {
				if ((rendezveny.getOsszBevetel() > legtobbBevetel)
						&& (rendezveny.getOsszBevetel() < elozoLegtobbBevetel)) {
					legtobbBevetel = rendezveny.getOsszBevetel();
					legNagyobbRendezveny = rendezveny;
				}

			}
			haromRendezveny.add(legNagyobbRendezveny);
			elozoLegtobbBevetel = legtobbBevetel;
			elozoLegtobbBevetel--;
			legtobbBevetel = 0;

		}
		
		ObservableList<Rendezveny> olLBRendezvenyei = FXCollections.observableArrayList(haromRendezveny);
		lvLBRendezvenyek.setItems(olLBRendezvenyei);

	}

}
