package vs.model;

public class Rendezveny {

	private String cim;
	private String idopont;
	private int jegyar;
	private int resztvevokSzama;
	private int osszBevetel;

	public Rendezveny(String cim, String idopont, int jegyar) {
		super();
		this.cim = cim;
		this.idopont = idopont;
		this.jegyar = jegyar;
	}

	public void resztVesz() {
		this.osszBevetel += this.jegyar;
	}

	public String getCim() {
		return cim;
	}

	public void setCim(String cim) {
		this.cim = cim;
	}

	public String getIdopont() {
		return idopont;
	}

	public void setIdopont(String idopont) {
		this.idopont = idopont;
	}

	public int getJegyar() {
		return jegyar;
	}

	public void setJegyar(int jegyar) {
		this.jegyar = jegyar;
	}

	public int getResztvevokSzama() {
		return resztvevokSzama;
	}

	public void setResztvevokSzama(int resztvevokSzama) {
		this.resztvevokSzama = resztvevokSzama;
	}

	public int getOsszBevetel() {
		return osszBevetel;
	}

	public void setOsszBevetel(int osszBevetel) {
		this.osszBevetel = osszBevetel;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(cim);
		builder.append(", ");
		builder.append(idopont);
		builder.append(", ");
		builder.append(jegyar);
		// builder.append(" Resztvett : ");
		// builder.append(resztvevokSzama);
		// builder.append(" ember. Ossz bevetel : ");
		// builder.append(osszBevetel);
		return builder.toString();
	}

}
