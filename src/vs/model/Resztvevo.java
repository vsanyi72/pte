package vs.model;

import java.util.ArrayList;

public class Resztvevo {

	private String nev;
	private String azonosito;
	private int penz;
	private ArrayList<Rendezveny> resztVettRendezvenyek = new ArrayList<>();

	public Resztvevo(String nev, String azonosito, int penz) {
		super();
		this.nev = nev;
		this.azonosito = azonosito;
		this.penz = penz;
	}
	
	public void penztKolt(int osszeg) {
		this.penz-=osszeg;
			
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getAzonosito() {
		return azonosito;
	}

	public void setAzonosito(String azonosito) {
		this.azonosito = azonosito;
	}

	public ArrayList<Rendezveny> getResztVettRendezvenyek() {
		return resztVettRendezvenyek;
	}

	public void setResztVettRendezvenyek(ArrayList<Rendezveny> resztVettRendezvenyek) {
		this.resztVettRendezvenyek = resztVettRendezvenyek;
	}

	public int getPenz() {
		return penz;
	}

	public void setPenz(int penz) {
		this.penz = penz;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(nev);
		builder.append(", ");
		builder.append(azonosito);
		builder.append(", ");
		builder.append(penz);
		return builder.toString();
	}

	
	
}
