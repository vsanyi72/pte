package vs.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import vs.model.Rendezveny;
import vs.model.Resztvevo;

class ResztvevoTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		Resztvevo resztvevo = new Resztvevo("GipszJakab","AZ1", 3000);
		assertEquals(resztvevo.getNev(), "GipszJakab");
		assertEquals(resztvevo.getAzonosito(), "AZ1");
		assertEquals(resztvevo.getPenz(), 3000);
		assertNotEquals(resztvevo.getPenz(), 3001);   
		resztvevo.penztKolt(1000);
		assertEquals(resztvevo.getPenz(), 2000);
		resztvevo.penztKolt(-1000);
		assertEquals(resztvevo.getPenz(), 3000);
		resztvevo.setPenz(-6);
		assertEquals(resztvevo.getPenz(), -6);
		resztvevo.penztKolt(-6);
		assertEquals(resztvevo.getPenz(), 0);
		resztvevo.getResztVettRendezvenyek().add(new Rendezveny("cim", "idopont", 1000));
		assertNotNull(resztvevo.getResztVettRendezvenyek().get(0));
		assertEquals(resztvevo.getResztVettRendezvenyek().size(), 1);
		resztvevo.getResztVettRendezvenyek().remove(0);
		assertEquals(resztvevo.getResztVettRendezvenyek().size(), 0);
	}

}
